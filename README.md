# Files microservice

## Endpoints

`POST /upload` - upload files. JS module
[fileapi](https://www.npmjs.com/package/fileapi) is used to upload
files.

`GET /` - info and healthcheck


## Environment variables

...

## Examples

```bash
# upload 1 file
curl -L -X POST -F 'file_name=@Cargo.toml' http://localhost:8080/upload
```

## Development

```bash
cargo install cargo-watch
cargo watch -x 'run'
```
