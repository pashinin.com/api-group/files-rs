# Build Stage
# docker build -t registry.gitlab.com/pashinin.com/api-group/files-rs:master .
FROM pashinin/rust-runtime:2020-09-21
COPY app ./
ENTRYPOINT ["/app"]
CMD ["./app"]
