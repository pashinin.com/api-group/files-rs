use actix_web::{web, Responder, get};
use serde::{Serialize};
use std::env;

#[derive(Serialize)]
struct Info<'a> {
    service: &'a str,
    version: &'a str,
    description: &'a str,
    commit_short_sha: &'a str,
}

#[get("/")]
pub async fn healthcheck() -> impl Responder {
    web::Json(Info{
        service: env!("CARGO_PKG_NAME"),
        version: env!("CARGO_PKG_VERSION"),
        description: env!("CARGO_PKG_DESCRIPTION"),
        commit_short_sha: match option_env!("CI_COMMIT_SHORT_SHA") {
            Some(sha) => sha,
            None => "",
        },
    })
}
