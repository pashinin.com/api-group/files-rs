use actix_multipart::Multipart;
use actix_web::{web, HttpResponse, HttpRequest, Error, post};
// use serde::{Deserialize};
use futures::{TryStreamExt};  // StreamExt

use crate::AppData;


#[post("/upload")]
pub async fn upload(
    _request: HttpRequest,
    mut payload: Multipart,
    _data: web::Data<self::AppData>,
) -> Result<HttpResponse, Error> {
    // iterate over multipart stream
    while let Ok(Some(field)) = payload.try_next().await {
        let content_type = field.content_disposition().unwrap();
        let filename = content_type.get_filename().unwrap();
        let _filepath = format!("./tmp/{}", sanitize_filename::sanitize(&filename));

        dbg!(filename);

        // File::create is blocking operation, use threadpool
        // let mut f = web::block(|| std::fs::File::create(filepath))
        //     .await
        //     .unwrap();

        // Field in turn is stream of *Bytes* object
        // while let Some(chunk) = field.next().await {
        //     let data = chunk.unwrap();
        //     // filesystem operations are blocking, we have to use threadpool
        //     f = web::block(move || f.write_all(&data).map(|_| f)).await?;
        // }
    }
    Ok(HttpResponse::Ok().into())
    // Ok(HttpResponse::from("asd"))
}
