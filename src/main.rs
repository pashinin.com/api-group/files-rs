mod endpoints;

use actix_web::{web, error, App, HttpServer, middleware, HttpResponse, HttpRequest};
// use std::env;

pub struct AppData {
    // token: String,
}

fn json_error_handler(err: error::JsonPayloadError, _req: &HttpRequest) -> error::Error {
    use actix_web::error::JsonPayloadError;

    let detail = err.to_string();
    println!("{}", detail);
    let resp = match &err {
        JsonPayloadError::ContentType => {
            HttpResponse::UnsupportedMediaType().body(detail)
        }
        JsonPayloadError::Deserialize(json_err) if json_err.is_data() => {
            HttpResponse::UnprocessableEntity().body(detail)
        }
        _ => HttpResponse::BadRequest().body(detail),
    };
    error::InternalError::from_response(err, resp).into()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // let token = env::var("DEPLOY_TOKEN").expect("You must set DEPLOY_TOKEN");

    HttpServer::new(move || {
        App::new()
            .data(AppData{
                // token: token.clone(),
            })
            .wrap(middleware::Logger::default())
            .service(endpoints::healthcheck)
            .service(endpoints::upload).app_data(
                web::JsonConfig::default().error_handler(json_error_handler),
            )
    })
        .bind("0.0.0.0:8080")?
        .run()
        .await
}
